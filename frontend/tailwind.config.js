const colors = require("tailwindcss/colors");

module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    colors: {
      "gray-main": "#F2F2F2",
      "date-card": "#666666",
      "gray-btn": "#CCCCCC",
      black: colors.black,
      white: colors.white,
      gray: colors.gray,
      emerald: colors.emerald,
      indigo: colors.indigo,
      yellow: colors.yellow,
      red: colors.red,
      amber: colors.amber,
      green: colors.green,
      purple: colors.purple,
      rose: colors.rose,
      stone: colors.stone,
    },
    extend: {},
  },
  plugins: [require("@tailwindcss/line-clamp")],
};
