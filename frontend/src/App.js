import { Route, Routes } from "react-router-dom";
import ScrollToTop from "./components/ScrollToTop/ScrollToTop";
import ApplyMessage from "./pages/apply/ApplyMessage";
import ApplyPage from "./pages/apply/ApplyPage";
import ContactPage from "./pages/contact/ContactPage";
import ContactMessage from "./pages/contact/ContactMessage";
import Home from "./pages/home/Home";
import NotFound from "./pages/NotFound/NotFound";
import Offer from "./pages/offerItem/Offer";
import Offers from "./pages/offers/Offers";

function App() {
  return (
    <>
      <ScrollToTop />
      <Routes>
        <Route index element={<Home />} />
        <Route path="contact" element={<ContactPage />} />
        <Route path="offers" element={<Offers />} />
        <Route path="offers/:offerId" element={<Offer />} />
        <Route path="/apply/message/:offerName" element={<ApplyMessage />} />
        <Route path="/contact/message" element={<ContactMessage />} />
        <Route path="apply/:offerTitle/:offerId" element={<ApplyPage />} />
        <Route path="*" element={<NotFound />} />
      </Routes>
    </>
  );
}

export default App;
