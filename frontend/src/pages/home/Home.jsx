import React from "react";
import Layout from "../../components/layout/Layout";
import Hero from "../../components/hero/Hero";
import AboutUs from "../../components/AboutUs/AboutUs";
import LinkFooter from "../../components/LinkFooter/LinkFooter";
import LastOffers from "../../components/LastOffers/LastOffers";
function Home() {
  return (
    <>
      <Layout>
        <Hero />
        <AboutUs />
        <LastOffers />
        <LinkFooter />
      </Layout>
    </>
  );
}

export default Home;
