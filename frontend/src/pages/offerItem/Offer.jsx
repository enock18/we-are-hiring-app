import axios from "axios";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Layout from "../../components/layout/Layout";
import OfferWrap from "../../components/OfferWrap/OfferWrap";
import PageLabel from "../../components/PageLabel/PageLabel";

function Offer() {
  const { offerId } = useParams();
  const [offer, setOffer] = useState({});
  const getOffer = async () => {
    const response = await axios(
      `${process.env.REACT_APP_LOCAL_HOST}/api/offers/${offerId}`
    );
    const data = response.data;
    setOffer(data);
  };

  useEffect(() => {
    getOffer();
  }, []);
  console.log(offer);
  return (
    <>
      <Layout>
        <PageLabel label={offer.title} />
        <OfferWrap
          date={offer.created_at}
          title={offer.title}
          contractType={offer.contractType}
          city={offer.city}
          jobDescription={offer.jobDescription}
          offerId={offerId}
        />
      </Layout>
    </>
  );
}

export default Offer;
