import React from "react";
import Layout from "../../components/layout/Layout";
import PageLabel from "../../components/PageLabel/PageLabel";

import MessageContainer from "../../components/Message/MessageContainer";

function ContactMessage() {
  return (
    <>
      <Layout>
        <PageLabel label="Apply" />
        <MessageContainer
          head="Thanks !"
          title="Your message has been sent ! "
          children="We thank you for your attention. We will respond as soon as possible. See you soon"
        />
      </Layout>
    </>
  );
}
export default ContactMessage;
