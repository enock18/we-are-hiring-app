import React from "react";
import Contact from "../../components/Contact/Contact";
import Layout from "../../components/layout/Layout";
import PageLabel from "../../components/PageLabel/PageLabel";

function ContactPage() {
  return (
    <>
      <Layout>
        <PageLabel label="contact us" />
        <Contact />
      </Layout>
    </>
  );
}

export default ContactPage;
