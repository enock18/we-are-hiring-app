import React from "react";
import Layout from "../../components/layout/Layout";
import PageLabel from "../../components/PageLabel/PageLabel";
import Apply from "../../components/apply/Apply";

function ApplyPage() {
  return (
    <>
      <Layout>
        <PageLabel label="Apply" />
        <Apply />
      </Layout>
    </>
  );
}
export default ApplyPage;
