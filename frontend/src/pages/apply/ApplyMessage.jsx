import React from "react";
import Layout from "../../components/layout/Layout";
import PageLabel from "../../components/PageLabel/PageLabel";
import { useParams } from "react-router-dom";
import MessageContainer from "../../components/Message/MessageContainer";

function ApplyMessage() {
  let { offerName } = useParams();
  return (
    <>
      <Layout>
        <PageLabel label="Apply" />
        <MessageContainer
          head="Thanks !"
          title={` Your applicatiopn to "${offerName}" offer  has been sent !`}
          children="We thank you for your application. We will respond as soon as possible. See you soon "
        />
      </Layout>
    </>
  );
}
export default ApplyMessage;
