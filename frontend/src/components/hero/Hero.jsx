import React from "react";
import ButtonFilled from "../Buttons/ButtonFilled";

function Hero() {
  return (
    <div className="flex-grow flex items-center justify-center h-96 bg-stone-200">
      <div className="flex flex-col items-center justify-center gap-2 md:w-1/2 ">
        <h1 className="text-3xl font-bold tracking-wide mb-5 md:text-5xl text-center">
          Finding a Job has never <br />
          been easier
        </h1>

        <p className="text-xl text-center font-normal w-100 mb-7">
          We hiring allows you to find the best job offers on the market and
          apply quickly and securely
        </p>
        <ButtonFilled link="/offers" name="See all offers" />
      </div>
    </div>
  );
}

export default Hero;
