import React, { useRef } from "react";
import { useParams } from "react-router-dom";
import { useNavigate } from "react-router-dom";

// components

import ButtonSubmit from "../Buttons/ButtonSubmit";

function Apply() {
  let firstNameInput = useRef(null);
  let lastNameInput = useRef(null);
  let emailInput = useRef(null);
  let phoneInput = useRef(null);
  let addressInput = useRef(null);
  let blocInput = useRef(null);
  let postalCodeInput = useRef(null);
  let cityNameInput = useRef(null);
  let linkedinUrlInput = useRef(null);
  let gitUrlInput = useRef(null);
  const { offerId, offerTitle } = useParams();

  const navigate = useNavigate();
  function sendApplication() {
    const application = {
      firstname: firstNameInput.current.value,
      lastname: lastNameInput.current.value,
      email: emailInput.current.value,
      address:
        postalCodeInput.current.value +
        " " +
        addressInput.current.value +
        " " +
        blocInput.current.value +
        " " +
        cityNameInput.current.value,
      offer: `/api/offers/${offerId}`,
      phoneNumber: phoneInput.current.value,
      profileLinkedin: linkedinUrlInput.current.value,
      profileGit: gitUrlInput.current.value,
    };

    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(application),
    };
    fetch(
      `${process.env.REACT_APP_LOCAL_HOST}/api/applications`,
      requestOptions
    ).then(function (data) {
      if (data.status === 201) {
        navigate(`/apply/message/${offerTitle}`);
      }
    });
  }

  return (
    <div className="w-9/12 m-auto mt-20">
      <h2 className="font-bold">Apply to</h2>
      <h1 className="font-bold text-3xl mb-3">{offerTitle}</h1>
      <form
        onSubmit={(e) => {
          e.preventDefault();
        }}
      >
        <label for="firstname" className="font-bold">
          Your first name <span className="text-red-500 ">*</span>
        </label>
        <input
          type="text"
          className="form-control
          block
          w-full
          px-3
          py-1.5
          text-base
          font-normal
          text-gray-700
          bg-white bg-clip-padding
          border border-solid border-gray-300
          rounded-lg
          transition
          ease-in-out
          mb-5
          mt-2
          focus:text-gray-700 focus:bg-white focus:border-black focus:outline-none"
          id="exampleInput124"
          aria-describedby="emailHelp124"
          name="firstname"
          placeholder="Edy"
          ref={firstNameInput}
          required
        />
        <label htmlFor="lastname" className="font-bold">
          Your last name <span className="text-red-500">*</span>
        </label>
        <input
          type="text"
          className="form-control
          block
          w-full
          px-3
          py-1.5
          text-base
          font-normal
          text-gray-700
          bg-white bg-clip-padding
          border border-solid border-gray-300
          rounded-lg
          transition
          ease-in-out
          mb-5 
          mt-2
          focus:text-gray-700 focus:bg-white focus:border-black focus:outline-none"
          id="exampleInput124"
          aria-describedby="emailHelp124"
          name="lastname"
          placeholder="Murphy"
          ref={lastNameInput}
          required
        />
        <label htmlFor="email" className="font-bold">
          Your email address <span className="text-red-500">*</span>
        </label>
        <input
          type="email"
          name="email"
          className="form-control
          block
          w-full
          px-3
          py-1.5
          text-base
          font-normal
          text-gray-700
          bg-white bg-clip-padding
          border border-solid border-gray-300
          rounded-lg
          transition
          ease-in-out
          mb-5
          mt-2
          focus:text-gray-700 focus:bg-white focus:border-black focus:outline-none"
          id="exampleInput124"
          aria-describedby="emailHelp124"
          placeholder="edymurphy@gmail.com"
          ref={emailInput}
          required
        />
        <label htmlFor="number" className="font-bold">
          Your phone number
        </label>
        <input
          type="phone"
          className="form-control
          block
          w-full
          px-3
          py-1.5
          text-base
          font-normal
          text-gray-700
          bg-white bg-clip-padding
          border border-solid border-gray-300
          rounded-lg
          transition
          ease-in-out
          mb-5
          mt-2
          focus:text-gray-700 focus:bg-white focus:border-black focus:outline-none"
          id="exampleInput124"
          aria-describedby="emailHelp124"
          name="phone"
          placeholder="+33 6 13 78 09 78"
          ref={phoneInput}
          required
        />

        <label htmlFor="adress" className="font-bold">
          Your address
        </label>
        <input
          type="text"
          name="address"
          className="form-control
          block
          w-full
          px-3
          py-1.5
          text-base
          font-normal
          text-gray-700
          bg-white bg-clip-padding
          border border-solid border-gray-300
          rounded-lg
          transition
          ease-in-out
          mb-5
          mt-2
          focus:text-gray-700 focus:bg-white focus:border-black focus:outline-none"
          id="exampleInput124"
          aria-describedby="emailHelp124"
          placeholder="775 alabama street"
          ref={addressInput}
          required
        />
        <input
          type="text"
          name="Bloc "
          className="form-control
          block
          w-full
          px-3
          py-1.5
          text-base
          font-normal
          text-gray-700
          bg-white bg-clip-padding
          border border-solid border-gray-300
          rounded-lg
          transition
          ease-in-out
          mb-5
          mt-2
          focus:text-gray-700 focus:bg-white focus:border-black focus:outline-none"
          id="exampleInput124"
          aria-describedby="emailHelp124"
          placeholder="Bloc c"
          ref={blocInput}
          required
        />
        <div className="grid grid-cols-2 gap-4">
          <input
            type="text"
            name="postal code"
            className="form-control
          block
          w-full
          px-3
          py-1.5
          text-base
          font-normal
          text-gray-700
          bg-white bg-clip-padding
          border border-solid border-gray-300
          rounded-lg
          transition
          ease-in-out
          mb-5
          focus:text-gray-700 focus:bg-white focus:border-black focus:outline-none"
            id="exampleInput124"
            aria-describedby="emailHelp124"
            placeholder="31 000"
            ref={postalCodeInput}
            required
          />
          <input
            type="text"
            name="City name"
            className="form-control
          block
          w-full
          px-3
          py-1.5
          text-base
          font-normal
          text-gray-700
          bg-white bg-clip-padding
          border border-solid border-gray-300
          rounded-lg
          transition
          ease-in-out
          mb-5
          focus:text-gray-700 focus:bg-white focus:border-black focus:outline-none"
            id="exampleInput124"
            aria-describedby="emailHelp124"
            placeholder="Toulouse"
            ref={cityNameInput}
            required
          />
        </div>

        <label htmlFor="linkedin" className="font-bold">
          Your linkedin profile
        </label>
        <input
          type="text"
          className="form-control
          block
          w-full
          px-3
          py-1.5
          text-base
          font-normal
          text-gray-700
          bg-white bg-clip-padding
          border border-solid border-gray-300
          rounded-lg
          transition
          ease-in-out
          mb-5
          mt-2
          focus:text-gray-700 focus:bg-white focus:border-black focus:outline-none"
          id="exampleInput124"
          aria-describedby="emailHelp124"
          name="linkedin"
          placeholder="https://..."
          ref={linkedinUrlInput}
          required
        />
        <label htmlFor="gitprofile" className="font-bold">
          Your Github / Gitlab profile
        </label>
        <input
          type="text"
          name="gitprofile"
          className="form-control
          block
          w-full
          px-3
          py-1.5
          text-base
          font-normal
          text-gray-700
          bg-white bg-clip-padding
          border border-solid border-gray-300
          rounded-lg
          transition
          ease-in-out
          mb-5
          mt-2
          focus:text-gray-700 focus:bg-white focus:border-black focus:outline-none"
          id="exampleInput124"
          aria-describedby="emailHelp124"
          placeholder="https://..."
          ref={gitUrlInput}
          required
        />

        <div className="text-center m-10">
          <ButtonSubmit
            name="Apply"
            onClick={() => {
              sendApplication();
            }}
          />
        </div>
      </form>
    </div>
  );
}

export default Apply;
