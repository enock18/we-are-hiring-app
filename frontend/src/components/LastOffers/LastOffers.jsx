import moment from "moment";
import React, { useEffect, useState } from "react";
import Card from "../Card/Card";

function LastOffers() {
  const [lastOffers, setLastOffers] = useState([]);
  useEffect(() => {
    fetch(`${process.env.REACT_APP_LOCAL_HOST}/api/offers`)
      .then((response) => response.json())
      .then((data) => {
        setLastOffers(data["hydra:member"].slice(-3));
      });
  }, []);

  return (
    <>
      <div className="bg-stone-200 py-16">
        <div className="text-center mb-10 pt-10">
          <h1 className="text-3xl  font-bold  md:text-3xl">Last Offers</h1>
        </div>

        <div className="md:flex-row md:justify-around flex flex-col justify-evenly items-center	gap-8">
          {lastOffers.map((offer) => (
            <Card
              key={offer.id}
              date={moment(offer.date).format("DD.MM.YYYY")}
              jobTitle={offer.title}
              jobDescription={offer.jobDescription}
              cityName={offer.city}
              contractType={offer.contractType}
              color="bg-white"
              offerName={offer.title}
              offerId={offer.id}
            />
          ))}
        </div>
      </div>
    </>
  );
}

export default LastOffers;
