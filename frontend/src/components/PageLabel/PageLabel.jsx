import React from "react";

function PageLabel(props) {
  return (
    <div className="bg-stone-200 flex justify-center items-center">
      <div className="container">
        <h1 className="py-20 px-5 text-5xl capitalize text-center">
          {props.label}
        </h1>
      </div>
    </div>
  );
}

export default PageLabel;
