import React from "react";
import ButtonOutline from "../Buttons/ButtonOutline";

function LinkFooter() {
  return (
    <div className="h-52 flex justify-around  flex-wrap items-center ">
      <div>
        <h4 className="text-3xl font-medium mb-5 md:text-4xl  ">
          Ready to get started
        </h4>
        <p className="text-3xl  font-normal   md:text-4xl"> Contact us</p>
      </div>
      <ButtonOutline link="/contact" name="Contact Us" />
    </div>
  );
}

export default LinkFooter;
