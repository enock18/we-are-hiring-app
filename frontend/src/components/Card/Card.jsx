import React from "react";
import { MdOutlineChevronRight } from "react-icons/md";
import { Link, Outlet } from "react-router-dom";

const Card = ({
  date,
  jobTitle,
  jobDescription,
  contractType,
  cityName,
  color,
  offerId,
}) => {
  return (
    <>
      <div
        className={`md:max-w-[25%] w-[78%] p-6 rounded-[15px] flex flex-col justify-center gap-1.5 shadow-md ${color}`}
      >
        <div>
          <div>
            <p className="text-[#666666] text-sm leading-relaxed">{date}</p>
            <p className="text-xl font-normal mb-4">{jobTitle}</p>
          </div>
          <p className="mb-9 font-normal text-gray-700 leading-relaxed line-clamp-3">
            {jobDescription}
          </p>
          <div className="flex justify-between w-full ">
            <p className="leading-normal font-normal">
              {cityName}-{contractType}
            </p>
            <Link to={`/offers/${offerId}`} className="flex items-center">
              See More <MdOutlineChevronRight />
            </Link>
          </div>
        </div>
      </div>
      <Outlet />
    </>
  );
};
export default Card;
