import React from "react";
import { test } from "../api/messages";

function Test() {
  return (
    <>
      <div className="flex flex-col h-screen">
        {test()}
      </div>
    </>
  );
}

export default Test;

// for Center message => className="flex flex-col h-screen"
