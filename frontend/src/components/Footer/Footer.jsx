import React from "react";

function Footer() {
  return (
    <>
      <footer className="flex flex-col items-center bg-stone-200 w-full py-1">
        <p className="text-lg py-3 md:py-4 font-bold md:text-2xl">We are hiring</p>
        <hr className="w-3/4  border-black" />
        <p className="py-3 md:py-4 text-black">&copy; 2022 &#124; We are Hiring</p>
      </footer>
    </>
  );
}

export default Footer;
