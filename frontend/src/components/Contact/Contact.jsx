import React from "react";
import { useRef } from "react";
import { useNavigate } from "react-router-dom";
// components
import ButtonSubmit from "../Buttons/ButtonSubmit";

function Contact() {
  let emailInput = useRef(null);
  let messageInput = useRef(null);

  let navigate = useNavigate();

  function sendMessage() {
    const message = {
      email: emailInput.current.value,
      messageContent: messageInput.current.value,
    };

    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(message),
    };
    fetch(
      `${process.env.REACT_APP_LOCAL_HOST}/api/messages`,
      requestOptions
    ).then(function (data) {
      if (data.status === 201) {
        navigate("/contact/message");
      } else {
        navigate("/contact");
      }
    });
  }
  return (
    <div className="container mx-auto">
      <form
        onSubmit={(e) => {
          e.preventDefault();
        }}
        className="flex flex-col gap-5 w-9/12 mx-auto my-16"
      >
        <label className="font-bold" htmlFor="email">
          Your email address
        </label>
        <input
          name="email"
          type="email"
          className="form-control
          block
          mb-2
          w-full
          px-3
          py-2
          text-base
          font-normal
          text-gray-700
          bg-white bg-clip-padding
          border border-solid border-gray-300
          rounded-lg
          transition
          ease-in-out
          focus:text-gray-700 focus:bg-white focus:border-black focus:outline-none"
          id="exampleInput124"
          aria-describedby="emailHelp124"
          ref={emailInput}
          required
        />
        <label className="font-bold" htmlFor="message">
          Your message
        </label>
        <textarea
          name="message"
          className="
            form-control
            block
            w-full
            px-3
            py-2
            text-base
            font-normal
            text-gray-700
            bg-white bg-clip-padding
            border border-solid border-gray-300
            rounded-lg
            transition
            ease-in-out
            focus:text-gray-700 focus:bg-white focus:border-black focus:outline-none"
          id="message"
          rows="12"
          ref={messageInput}
          required
        />
        <div className="text-center mt-10 mb-10	">
          <ButtonSubmit
            onClick={() => {
              sendMessage();
            }}
            name="Send"
          />
        </div>
      </form>
    </div>
  );
}

export default Contact;
