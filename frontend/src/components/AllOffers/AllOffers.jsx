import moment from "moment";
import React, { useEffect, useState } from "react";
import Card from "../Card/Card";

// import Pagination from "react";

function AllOffers() {
  const [allOffers, setAllOffers] = useState([]);
  const [data, setData] = useState({});
  const [btnDisabled, setBtnDisabled] = useState(false);
  const [btnDisabledNext, setBtnDisabledNext] = useState(false);
  useEffect(() => {
    fetchOffer("/api/offers");
    setBtnDisabled(true);
    window.scrollTo(0, 0);
  }, []);

  const fetchOffer = async function (url) {
    const response = await fetch(`${process.env.REACT_APP_LOCAL_HOST}${url}`);
    const data = await response.json();
    setAllOffers(data["hydra:member"]);
    setData(data);
  };

  const next = () => {
    if (data["hydra:view"]["hydra:next"]) {
      fetchOffer(data["hydra:view"]["hydra:next"]);
      setBtnDisabledNext(false);
      setBtnDisabled(false);
    } else {
      fetchOffer(data["hydra:view"]["@id"]);
      setBtnDisabledNext(true);
    }
  };

  const prev = () => {
    if (data["hydra:view"]["hydra:previous"]) {
      fetchOffer(data["hydra:view"]["hydra:previous"]);
      setBtnDisabled(false);
      setBtnDisabledNext(false);
    } else {
      fetchOffer(data["hydra:view"]["@id"]);
      setBtnDisabled(true);
    }
  };

  return (
    <>
      <div className="bg-white py-16 ">
        <div className="w-full gap-14 md:flex-row flex flex-col items-center md:flex-wrap justify-center">
          {allOffers.map((offer, index) => (
            <Card
              key={index}
              date={moment(offer.date).format("DD.MM.YYYY")}
              jobTitle={offer.title}
              jobDescription={offer.jobDescription}
              cityName={offer.city}
              contractType={offer.contractType}
              color="bg-stone-200"
              offerId={offer.id}
            />
          ))}
        </div>
      </div>
      <div className="flex md:justify-center py-16 font-normal md:text-2xl text-xl justify-between">
        <button
          type="button"
          onClick={prev}
          className={
            btnDisabled ? "text-gray-btn md:mr-20 mr-0" : "md:mr-20 mr-0"
          }
        >
          {"<<"} Previous page
        </button>
        <button
          type="button"
          onClick={next}
          className={
            btnDisabledNext ? "text-gray-btn md:ml-10 ml-0" : "md:ml-10 ml-0"
          }
        >
          Next page {">>"}
        </button>
      </div>
    </>
  );
}

export default AllOffers;
