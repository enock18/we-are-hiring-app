import React from "react";

function MessageContainer(props) {
  return (
    <div className="container mx-auto flex-grow flex items-center justify-center mb-16">
      <div className="flex flex-col items-center justify-center gap-2 p-5">
        <h1 className="text-5xl font-bold tracking-wide">{props.head}</h1>
        <h2 className="text-2xl font-bold tracking-wide text-center">
          {props.title}
        </h2>
        <p className="text-xl text-center font-normal">{props.children}</p>
      </div>
    </div>
  );
}

export default MessageContainer;
