import React from "react";

function AboutUs() {
  return (
    <div className="h-80 mx-auto flex-grow flex items-center justify-center">
      <div className="flex flex-col items-center justify-center gap-2 p-5 md:w-1/2">
        <h2 className="text-3xl font-bold tracking-wide mb-5 md:text-4xl">
          About Us
        </h2>

        <p className="text-xl text-center font-normal ">
          lorem ipsum dolor sit amet, consectetur adispicing elit, sed do
          eiusmod tempor incididunt utlorem ,consectetur adispicing sed do
          eiusmod tempor
        </p>
      </div>
    </div>
  );
}

export default AboutUs;
