import React from "react";
import { FiChevronLeft } from "react-icons/fi";
import { Link } from "react-router-dom";
import ButtonFilled from "../../components/Buttons/ButtonFilled";
import moment from "moment";

function OfferWrap({
  date,
  title,
  contractType,
  city,
  jobDescription,
  offerId,
}) {
  return (
    <>
      <div className="container mx-auto">
        <div className="sm:w-10/12 sm:mx-auto flex flex-col gap-7 px-3 pb-12 pt-14 tracking-wide md:py-16 md:gap-12">
          <div className="flex justify-between items-center text-xl md:text-2xl md:my-2">
            <p className="font-semibold">{title}</p>
            <div className="font-semibold flex flex-col items-end">
              <p>{contractType}</p>
              <p className="capitalize">{city}</p>
            </div>
          </div>
          <p>{jobDescription}</p>
          <p className="font-semibold text-gray-500 mt-1 mb-6 md:mt-0">
            Published on {moment(date).format("DD.MM.YYYY")}
          </p>
          <div className="flex flex-col gap-6 items-center md:gap-8">
            <ButtonFilled
              link={`/apply/${title}/${offerId}`}
              name="Apply now"
            />
            <Link className="flex items-center" to="/offers">
              {" "}
              <FiChevronLeft />
              Back to offers
            </Link>
          </div>
        </div>
      </div>
    </>
  );
}

export default OfferWrap;
