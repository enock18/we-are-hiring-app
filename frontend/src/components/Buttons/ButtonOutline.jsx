import React from "react";
import { Link } from "react-router-dom";
function ButtonOutline(props) {
  return (
    <Link
      className="inline-block bg-transparent tracking-wider text-black px-20 py-3 text-md font-semibold border border-black uppercase rounded hover:bg-black hover:text-white "
      to={props.link}
    >
      {props.name}
    </Link>
  );
}

export default ButtonOutline;
