import React from "react";
import { Link } from "react-router-dom";

function ButtonFilled(props) {
  return (
    <Link
      className="inline-block px-20 py-3 text-md tracking-wider font-semibold bg-black text-white rounded shadow-md"
      to={props.link}
    >
      {props.name}
    </Link>
  );
}

export default ButtonFilled;
