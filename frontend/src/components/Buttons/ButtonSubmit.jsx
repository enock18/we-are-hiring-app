import React from "react";

function ButtonSubmit(props) {
  return (
    <button
      onClick={props.onClick}
      className="inline-block px-20 py-3 text-md font-semibold bg-black text-white uppercase rounded shadow-md"
      type="submit"
    >
      {props.name}
    </button>
  );
}

export default ButtonSubmit;
