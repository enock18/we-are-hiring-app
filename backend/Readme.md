# Hiring app - Backend

## Requirements

Note: this project is written for linux and symfony 6, adapt accordingly

Before creating your first Symfony application (version 6.x) you must:

- Install PHP 8.0.2 or higher and these PHP extensions (which are installed and enabled by default in most PHP 8 installations): Ctype, iconv, PCRE, Session, SimpleXML, and Tokenizer;

- Install Composer, which is used to install PHP packages.

## Getting Started

1. Clone the repository with the following command.

```
git clone git clone git@gitlab.com:arsene.ntibus/we-are-hiring.git
```

2. Enter in the folder of project

```
cd we-are-hiring/backend
```

3. Run these following commands to install "Composer" and "Node" dependencies.

```
npm i
composer install
```

```
composer install

```

```
npm run build

```

4.In the root folder, create a .env.local file in which you will indicate the address of your PHPMYADMIN server as follows

```
DATABASE_URL="mysql://USERNAME:PASSWORD@127.0.0.1:3306/DBNAME?serverVersion=5.7&charset=utf8mb4"
```

5.  Create a database for the project

```
symfony console doctrine:database:create
```

6.  Make migrations in order to create database structure ( tables )

```
symfony console doctrine:migrations:migrate
```

7.  Load fixtures in order to add fake data in the database (You will need to add admin info in the database in order that login page for backoffice works well.)

```
symfony console doctrine:fixtures:load
```

Note : Part 6 will add a username,password and role admin in the table of admin to help you to login , if you want to change this password or username you need to go
`src/DataFixtures` and change values.

8.  Launch your server with the following command.

```
symfony server:start
or
php -S localhost:8000 -t public/
```

9.  In order to access the API documentation go to

```
localhost:8000/api
```

To access the administration panel

```
localhost:8000/admin
```

```
username:admin
password:password
```
