<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\OfferRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: OfferRepository::class)]
#[ApiResource(
    attributes: ["pagination_enabled" => true],

    collectionOperations: [
        'get' => [
            'normalization_context' => ['groups' => ['read:item']]
        ],


    ],
    itemOperations: [
        'get' => [
            'normalization_context' => ['groups' => ['read:collection']]
        ]
    ],
    paginationItemsPerPage: 6,
    paginationMaximumItemsPerPage: 6
)]
class Offer
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['read:collection', 'read:item'])]
    private $id;

    #[ORM\Column(type: 'string', length: 100)]
    //Validators
    #[Assert\NotBlank(
        message: 'Please enter a job title.',
    )]
    #[Assert\length(
        min: 5,
        max: 100,
        minMessage: 'The title must be at least {{ limit }} characters long.',
        maxMessage: 'The title must be less than {{ limit }} characters long.'
    )]
    #[Groups(['read:collection', 'read:item'])]
    private ?string $title;

    #[ORM\Column(type: 'string', length: 50)]
    //Validators
    #
    #[Assert\NotBlank(
        message: 'Please enter a city name.',
    )]
    #[Assert\Regex(
        pattern: '/^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/i',
    )]
    #[Groups(['read:collection', 'read:item'])]
    private ?string $city;

    #[ORM\Column(type: 'text', length: 2500)]
    //Validators
    #[Assert\NotBlank(
        message: 'Please enter a job description.'
    )]
    #[Assert\length(
        min: 10,
        max: 2500,
        minMessage: 'The Job description must be at least {{ limit }} characters long.',
        maxMessage: 'The Job description must be less than {{ limit }} characters long.'
    )]
    #[Groups(['read:collection', 'read:item'])]
    private ?string $jobDescription;

    #[ORM\Column(type: 'string', length: 100)]
    #[Assert\NotBlank(
        message: 'The contract type field should not be empty.'
    )]
    #[Assert\Type(type: "alpha", message: "The value {{ value }} is not a valid {{ type }}.")]
    #[Assert\length(
        min: 3,
        max: 100,
        minMessage: 'The contract type must be at least {{ limit }} characters long.',
        maxMessage: 'The contract type must be less than {{ limit }} characters long.',
    )]
    #[Groups(['read:collection', 'read:item'])]
    private ?string $contractType;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups(['read:collection', 'read:item'])]
    private $created_at;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups(['read:collection', 'read:item'])]
    private $updated_at;

    #[ORM\OneToMany(mappedBy: 'offer', targetEntity: Application::class, orphanRemoval: true)]
    #[ORM\JoinColumn(nullable: true)]
    private $applications;

    public function __construct()
    {
        $this->applications = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOffer(): ?string
    {
        return $this->offer;
    }

    public function setOffer(string $Offer): self
    {
        $this->offer = $Offer;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getJobDescription(): ?string
    {
        return $this->jobDescription;
    }

    public function setJobDescription(string $jobDescription): self
    {
        $this->jobDescription = $jobDescription;

        return $this;
    }

    public function getContractType(): ?string
    {
        return $this->contractType;
    }

    public function setContractType(string $contractType): self
    {
        $this->contractType = $contractType;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->created_at = $createdAt;

        return $this;
    }

    public function getUpdatedAT(): ?\DateTimeImmutable
    {
        return $this->updated_at;
    }

    public function setUpdatedAT(?\DateTimeImmutable $updatedAT): self
    {
        $this->updated_at = $updatedAT;

        return $this;
    }

    /**
     * @return Collection<int, Application>
     */
    public function getApplications(): Collection
    {
        return $this->applications;
    }

    public function addApplication(Application $application): self
    {
        if (!$this->applications->contains($application)) {
            $this->applications[] = $application;
            $application->setOffer($this);
        }

        return $this;
    }

    public function removeApplication(Application $application): self
    {
        if ($this->applications->removeElement($application)) {
            // set the owning side to null (unless already changed)
            //getOfferId()
            if ($application->getOffer() === $this) {
                $application->setOffer(null);
            }
        }
        return $this;
    }
    public function __toString()
    {
        return $this->title;
    }
}
