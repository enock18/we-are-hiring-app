<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use App\Repository\MessageRepository;
use ApiPlatform\Core\Action\NotFoundAction;
use ApiPlatform\Core\Annotation\ApiResource;

#[ORM\Entity(repositoryClass: MessageRepository::class)]
#[ApiResource(
    collectionOperations: [
        'post'
    ],
    itemOperations: [
        'get' => [
            'controller' => NotFoundAction::class,
            'read' => false,
            'output' => false,
        ],
    ]
)]
class Message
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id;
    #[ORM\Column(type: 'string', length: 100)]
    // Validators
    #[Assert\Email(mode: "strict", message: "The email is not a valid.")]
    #[Assert\NotBlank(message: "Please enter an email.")]
    #[Assert\Length(min: 6, max: 100, minMessage: "The email must be at least {{ limit }} characters long.", maxMessage: "The email cannot be longer than {{ limit }} characters.")]
    private $email;

    #[ORM\Column(type: 'text', length: 2500)]
    // Validators
    #[Assert\NotBlank(message: "Please write your message.")]
    #[Assert\Length(
        min: 5,
        max: 2500,
        minMessage: "Your message must be at least {{ limit }} characters long.",
        maxMessage: "Your message must be less than {{ limit }} characters long.",
    )]
    private $messageContent;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Gedmo\Timestampable(on: "create")]
    private $created_at;
    public function getId(): ?int
    {
        return $this->id;
    }
    public function getEmail(): ?string
    {
        return $this->email;
    }
    public function setEmail(string $email): self
    {
        $this->email = $email;
        return $this;
    }
    public function getMessageContent(): ?string
    {
        return $this->messageContent;
    }
    public function setMessageContent(string $messageContent): self
    {
        $this->messageContent = $messageContent;

        return $this;
    }
    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->created_at;
    }
    public function setCreatedAt(\DateTimeImmutable $created_at): self
    {
        $this->created_at = $created_at;
        return $this;
    }
}
