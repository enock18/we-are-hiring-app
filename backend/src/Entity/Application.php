<?php

namespace App\Entity;

use App\Entity\Offer;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use App\Repository\ApplicationRepository;
use ApiPlatform\Core\Action\NotFoundAction;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ApplicationRepository::class)]
#[ApiResource(

    collectionOperations: [
        'post'
    ],
    itemOperations: [
        'get' => [
            'controller' => NotFoundAction::class,
            'read' => false,
            'output' => false,
        ],
    ]
)]
class Application
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    // #[Groups(['write:publication','read:application'])]
    private $id;

    #[ORM\Column(type: 'string', length: 50)]
    // Validators
    #[Assert\Length(min: 2, max: 50, minMessage: "Your firstname must be at least {{ limit }} characters long.", maxMessage: "Your firstname cannot be longer than {{ limit }} characters.")]
    #[Assert\NotBlank(message: 'Please enter the firstname')]
    #[Assert\Regex(pattern: '/^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/i', message: 'The firstname is not valid, a firstname can only contain letters and one space between letters. ')]
    private $firstname;

    #[ORM\Column(type: 'string', length: 70)]
    // Validators
    #[Assert\Length(min: 2, max: 70, minMessage: "Your lastname must be at least {{ limit }} characters long.", maxMessage: "Your lastname cannot be longer than {{ limit }} characters.")]
    #[Assert\NotBlank(message: 'Please enter the lastname')]
    #[Assert\Regex(pattern: '/^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/i', message: 'The lastname is not valid, a lastname can only contain letters and one space between letters. ')]
    private $lastname;

    #[ORM\Column(type: 'string', length: 100)]
    // Validators
    #[Assert\Email(mode: "strict", message: "The email is not a valid.")]
    #[Assert\NotBlank(message: "Please enter an email")]
    #[Assert\Length(min: 6, max: 100, minMessage: "The email must be at least {{ limit }} characters long.", maxMessage: "The email cannot be longer than {{ limit }} characters.")]
    private $email;

    #[ORM\Column(type: 'string', length: 13)]
    // Validators
    #[Assert\NotBlank]
    #[Assert\Regex(pattern: '/^(?:(?:\+|00)33|0)\s*[1-9](?:[\s.-]*\d{2}){4}$/i', message: 'The phone number is not valid, a phone number can follow these patterns ( 0XXXXXXXXX or +33XXXXXXXXX )')]
    private $phone_number;

    #[ORM\Column(type: 'string', length: 255)]
    // Validators
    #[Assert\NotBlank]
    private $address;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    // Validators
    #[Assert\Url(message: 'The url {{ value }} is not a valid url', protocols: ['http', 'https'],)]
    private $profile_linkedin;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    // Validators
    #[Assert\Url(message: 'The url {{ value }} is not a valid url', protocols: ['http', 'https'],)]
    private $profile_git;

    #[ORM\ManyToOne(targetEntity: Offer::class, inversedBy: 'applications')]
    #[ORM\JoinColumn(nullable: false, onDelete: "CASCADE")]
    // Validators
    #[Assert\NotBlank]
    private $offer;

    
    #[ORM\Column(type: 'datetime_immutable')]
    // #[Groups(['write:publication'])]
    #[Gedmo\Timestampable(on:"create")]
    private $created_at;

    public function getId(): ?int
    {
        return $this->id;
    }
    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phone_number;
    }

    public function setPhoneNumber(string $phone_number): self
    {
        $this->phone_number = $phone_number;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getProfileLinkedin(): ?string
    {
        return $this->profile_linkedin;
    }

    public function setProfileLinkedin(string $profile_linkedin): self
    {
        $this->profile_linkedin = $profile_linkedin;

        return $this;
    }

    public function getProfileGit(): ?string
    {
        return $this->profile_git;
    }

    public function setProfileGit(?string $profile_git): self
    {
        $this->profile_git = $profile_git;

        return $this;
    }

    public function getOffer(): ?Offer
    {
        return $this->offer;
    }

    public function setOffer(?Offer $offer): self
    {
        $this->offer = $offer;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeImmutable $created_at): self
    {
        $this->created_at = $created_at;
        return $this;
    }
}