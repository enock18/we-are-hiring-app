<?php

namespace App\Controller;

use App\Entity\Application;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PostApplicationController extends AbstractController
{
   
   public function __invoke(Application $application): Application
   {
        return $application;
   }
   
    // // #[Route('/post/application', name: 'app_post_application')]
    // public function index(): Response
    // {
    //     return $this->render('post_application/index.html.twig', [
    //         'controller_name' => 'PostApplicationController',
    //     ]);
    // }
}