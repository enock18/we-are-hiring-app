<?php

namespace App\Controller\Admin;

use App\Entity\Offer;
use App\Entity\Message;
use App\Entity\Application;
use App\Controller\Admin\OfferCrudController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;

class DashboardController extends AbstractDashboardController
{
    public function __construct(private AdminUrlGenerator $adminUrlGenerator)
    {
        // $this->adminUrlGenerator = $adminUrlGenerator;
    }

    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        // return parent::index();
        $url = $this->adminUrlGenerator->setController(OfferCrudController::class)->generateUrl();
        return $this->redirect($url);

        // Option 1. You can make your dashboard redirect to some common page of your backend
        //
        // $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);

        // Option 2. You can make your dashboard redirect to different pages depending on the user
        //
        // if ('jane' === $this->getUser()->getUsername()) {
        //     return $this->redirect('...');
        // }

        // Option 3. You can render some custom template to display a proper dashboard with widgets, etc.
        // (tip: it's easier if your template extends from @EasyAdmin/page/content.html.twig)
        //
        // return $this->render('some/path/my-dashboard.html.twig');
    }

    // Configurations fot Dashboard menu
    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->renderSidebarMinimized()
            ->setTitle('Admin Dashboard');
    }

    // Configurations for items in Dashboard menu
    public function configureMenuItems(): iterable
    {
        // Section offers in sidebar of admin Dashboard
        yield MenuItem::section('Offers');
        yield MenuItem::subMenu('Actions', 'fas fa-briefcase')
            ->setSubItems([
                MenuItem::linkToCrud('Show Offers', 'fas fa-eye', Offer::class),
                MenuItem::linkToCrud('Add Offer', 'fas fa-plus', Offer::class)
                    ->setAction(Crud::PAGE_NEW),
            ]);

        // Section messages in sidebar of admin Dashboard
        yield MenuItem::section('Messages');
        yield MenuItem::subMenu('Actions', 'far fa-envelope')
            ->setSubItems([
                MenuItem::linkToCrud('Show Messages', 'fas fa-eye', Message::class),
                MenuItem::linkToCrud('Add Message', 'fas fa-plus', Message::class)
                    ->setAction(Crud::PAGE_NEW),
            ]);

        // Section application in sidebar of admin Dashboard
        yield MenuItem::section('Applications');
        yield MenuItem::subMenu('Actions', 'fas fa-file-signature')
            ->setSubItems([
                MenuItem::linkToCrud('Show Applications', 'fas fa-eye', Application::class),
                MenuItem::linkToCrud('Add Application', 'fas fa-plus', Application::class)
                    ->setAction(Crud::PAGE_NEW),
            ]);
    }
}