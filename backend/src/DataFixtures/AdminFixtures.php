<?php

namespace App\DataFixtures;

use App\Entity\Admin;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

// create an example of admin in database in order to login in Back office
class AdminFixtures extends Fixture
{
    private $username = "admin";
    private $role = ["ROLE_ADMIN"];
    private $password = '$2y$13$Bnw0JzghWiF8hBgcgOxvouSIkAXy7H2dQi12LXRCzqw1VJGhX1QKu'; // the password will be "password"

    public function load(ObjectManager $manager): void
    {
        $admin = new Admin();
        $admin->setUsername($this->username);
        $admin->setRoles($this->role);
        $admin->setPassword($this->password);
        $manager->persist($admin);

        $manager->flush();
    }
}